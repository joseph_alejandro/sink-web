import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Dashboard.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: { name: "inicio" },
    component: Home,
    children: [
      {
        path: "/inicio",
        name: "inicio",
        component: () => import("@/Components/Galeria/Inicio.vue"),
      },
      {
        path: "/exposiciones",
        component: () => import("@/Components/Galeria/Exposiciones.vue"),
      },
      {
        path: "/exposicionObras/:id",
        name: "ObrasExposicion",
        component: () => import("@/Components/Galeria/ExposicionObras.vue"),
      },
      {
        path: "/obras",
        component: () => import("@/Components/Galeria/ObrasEnGaleria.vue"),
      },
      {
        path: "/obra/:id",
        name: "Obras",
        component: () => import("@/Components/Galeria/Obras.vue"),
      },
      {
        path: "/artistas",
        component: () => import("@/Components/Galeria/Artistas.vue"),
      },
      {
        path: "/artista/:id",
        name: "Artista",
        component: () => import("@/Components/Galeria/Artista.vue"),
      },
      {
        path: "/contacto",
        component: () => import("@/Components/Galeria/Contacto.vue"),
      },
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
